package JFXCalc;

import Exceptions.SqrtFromMinusException;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.lang.Math.*;

public final class MyCalculations {

    public static double plus(double a, double b){
        return a+b;
    }

    public static double subtract(double a, double b){
        return a-b;
    }

    public static double multiply(double a, double b){
        return a*b;
    }

    public static double division(double a, double b) throws ArithmeticException{
        if (b==0) throw new ArithmeticException();
        return a/b;
    }

    public static double squareRoot(double a) throws SqrtFromMinusException{
        if (a<0) throw new SqrtFromMinusException();
        return sqrt(a);
    }

    public static double power(double a,double b){
        double res = pow(a,b);
        if (Double.isNaN(res)) throw new SqrtFromMinusException();
        return res;
    }
}
