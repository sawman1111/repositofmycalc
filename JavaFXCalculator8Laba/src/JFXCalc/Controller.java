package JFXCalc;

import Exceptions.SqrtFromMinusException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class Controller {
    @FXML
    private TextField textFieldCalc;
    private final String DEFAULT_ZERO = "0";
    private boolean isDigitWithComma;
    private boolean wasOperation;
    public boolean was1BinOperation;
    private double result = 0;
    private double secondres;
    private Button tmpForDigit;
    private Alert alert;
    private String operatorCalc = "";

    private void binOperation(String s){ //метод бинарной операции в зависимости от её типа
        if (textFieldCalc.getText().equals("")) result = 0;
        else result = Double.parseDouble(textFieldCalc.getText());
        if (!was1BinOperation) {
            operatorCalc = s;
            textFieldCalc.setText(DEFAULT_ZERO);
            secondres = result;
            //result = 0;
            was1BinOperation = true;
//        }else if (operatorCalc.equals(s)){
        }else{
            secondres = getResultOperation();
            textFieldCalc.setText(String.valueOf(secondres));
            operatorCalc = s;
            result = secondres;
        }
        isDigitWithComma = false;
//        else {
//            operatorCalc = s; а жаль, ибо у меня красиво получилось...
//        }
        wasOperation = true;
       // isDigitWithComma = true;
    }

    private double getResultOperation(){// метод выполнения операции
        double res = 0;
        switch (operatorCalc){
            case "+":
                res=MyCalculations.plus(secondres,result);
                break;
            case "-":
                res = MyCalculations.subtract(secondres,result);
                break;
            case "/":
                try {
                    res = MyCalculations.division(secondres,result);
                } catch (ArithmeticException e) {
                    alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Error!");
                    alert.setHeaderText("You try divide by 0!");
                    alert.showAndWait();
                }
                break;
            case "*":
                res = MyCalculations.multiply(secondres,result);
                break;
            case "^":
                try {
                    res = MyCalculations.power(secondres,result);
                } catch (Exception e) {
                    alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Error!");
                    alert.setHeaderText("You try to take incorrect power!");
                    alert.showAndWait();
                }
                break;
            default:
                break;
        }
        return res;
    }

    public void keyPressed(KeyEvent keyEvent){
        //textFieldCalc.requestFocus();
        //if (textFieldCalc.getText().equals("0")) textFieldCalc.setText("");
        KeyCode keyCode = keyEvent.getCode();
        //String s = keyCode.getChar();
        switch (keyCode) {
            case NUMPAD0:
                funcOut("0");
                break;
            case NUMPAD1:
                funcOut("1");
                break;
            case NUMPAD2:
                funcOut("2");
                break;
            case NUMPAD3:
                funcOut("3");
                break;
            case NUMPAD4:
                funcOut("4");
                break;
            case NUMPAD5:
                funcOut("5");
                break;
            case NUMPAD6:
                funcOut("6");
                break;
            case NUMPAD7:
                funcOut("7");
                break;
            case NUMPAD8:
                funcOut("8");
                break;
            case NUMPAD9:
                funcOut("9");
                break;
            case ADD:
                buttPlusClick();
                break;
            case SUBTRACT:
                buttMinusClick();
                break;
            case MULTIPLY:
                buttMultiplyClick();
                break;
            case P:
                buttPowClick();
                break;
            case DIVIDE:
                buttDivisionClick();
                break;
            case DECIMAL:
                buttCommaClick();
                break;
            case BACK_SPACE:
                buttBackSpaceClick();
                break;
            case ENTER:
                buttResultClick();
                break;
            case DELETE:
                buttClearClick();
                break;
            case MINUS:
                buttMinusPlusClick();
                break;
            case O:
                buttSQRTClick();
                break;
            default:
                break;
        }
    }

    public void buttResultClick(){
        if (!textFieldCalc.getText().equals("")) {
            result = Double.parseDouble(textFieldCalc.getText());
            double tmp = getResultOperation();
            if (tmp != 0) {
                result = tmp;
                textFieldCalc.setText(String.valueOf(result));
                operatorCalc = "";
                wasOperation = true;
                was1BinOperation = false;
                isDigitWithComma = true;
            }
        }
    }

    public void buttDivisionClick() {
        binOperation("/");
    }

    public void buttMinusClick(){
        binOperation("-");
    }

    public void buttPowClick(){
        binOperation("^");
    }

    public void buttMultiplyClick(){
        binOperation("*");
    }

    public void buttPlusClick(){
        binOperation("+");
    }

    public void buttSQRTClick(){
        try {
            result = Double.parseDouble(textFieldCalc.getText());
            textFieldCalc.setText(MyCalculations.squareRoot(result) + "");
            isDigitWithComma = true;
        } catch (SqrtFromMinusException e) {
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!");
            alert.setHeaderText("You try to take sqrt from negative counter!");
           // alert.setContentText("I have a great message for you!");
            alert.showAndWait();
        }
    }

    public void buttCommaClick(){
        if (!isDigitWithComma){
            textFieldCalc.appendText(".");
            isDigitWithComma = true;
        }
    }

    public void buttMinusPlusClick(){
        result = Double.parseDouble(textFieldCalc.getText());
        if (result!=0) {
            result = -result;
            textFieldCalc.setText(result + "");
            isDigitWithComma = true;
        }
    }

    public void buttClearClick(){
        textFieldCalc.setText(DEFAULT_ZERO);
        result = 0;
        isDigitWithComma = false;
        operatorCalc = "";
        wasOperation = false;
        was1BinOperation = false;
}

    public void buttBackSpaceClick(){
        int length = textFieldCalc.getText().length();
    if (!textFieldCalc.getText().equals(DEFAULT_ZERO) && length!=0){
        textFieldCalc.deleteText(length-1,length);
        if (!textFieldCalc.getText().contains(".")){
            isDigitWithComma = false;
        }
        if (textFieldCalc.getText().length()==0 || textFieldCalc.getText().equals("-")){
            textFieldCalc.setText(DEFAULT_ZERO);
            isDigitWithComma = false;
        }
    }
}

    public void buttDigitClick(ActionEvent actionEvent){
    tmpForDigit = (Button) actionEvent.getSource();
    funcOut(tmpForDigit.getText());
}

    private void funcOut(String s){
    if (textFieldCalc.getText().equals("0") || wasOperation ){
        textFieldCalc.setText(s);
        wasOperation = false;
    }else {
        textFieldCalc.appendText(s);
    }
}
}